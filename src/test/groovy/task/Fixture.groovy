package task

import task.domain.User
import task.domain.Website

import java.security.SecureRandom

/**
 * Stores common test data
 */
class Fixture {
    static def website = new Website(id: 1, name: "Google", url: "google.com", status: 200)
    static def token = "123"
    static def user = new User(username: "john", email: "john@mail.com", password: "foo")
    static def registeredUser = new User(username: user.username, password: user.password)
    private static def random = new SecureRandom()
    static def generateRandomUser() {
        return new User(username: random.nextInt().toString(),
                            email: random.nextInt().toString(),
                            password: random.nextInt().toString())
    }
}

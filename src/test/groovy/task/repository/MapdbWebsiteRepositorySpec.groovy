package task.repository

import org.mapdb.DB
import org.mapdb.HTreeMap
import spock.lang.Specification

import static task.Fixture.token
import static task.Fixture.website

class MapDBWebsiteRepositorySpec extends Specification {

    private DB db
    private HTreeMap tokenToWebsites
    private WebsiteRepository websiteRepository

    def setup() {
        db = Mock()
        tokenToWebsites = Mock()
        websiteRepository = new MapDBWebsiteRepository(usernameToWebsites: tokenToWebsites, db: db)
    }

    def "listWebsites() should return list of user's websites"() {
        when:
            def actualWebsites = websiteRepository.listWebsites(token)
        then:
            1 * tokenToWebsites.get(token) >> [website]
            actualWebsites.equals([website])
    }

    def "addWebsite() should add new user website"() {
        when:
            websiteRepository.addWebsite(token, website)
        then:
            1 * tokenToWebsites.get(token) >> []
            1 * tokenToWebsites.put(token, [website])
            1 * db.commit()
    }

    def "removeWebsite() should remove user's website"() {
        when:
            websiteRepository.removeWebsite(token, website.id)
        then:
            1 * tokenToWebsites.get(token) >> [website]
            1 * tokenToWebsites.put(token, [])
            1 * db.commit()
    }
}

package task.repository

import com.google.common.cache.LoadingCache
import org.mapdb.DB
import org.mapdb.HTreeMap
import spock.lang.Specification

import static task.Fixture.user
import static task.Fixture.token

class MapDBUserRepositorySpec extends Specification {

    private DB db
    private HTreeMap tokenToWebsites
    private HTreeMap usernameToUser
    private UserRepository userRepository
    private LoadingCache<String, String> tokenToUsername

    def setup() {
        db = Mock()
        tokenToWebsites = Mock()
        usernameToUser = Mock()
        tokenToUsername = Mock()
        userRepository = new MapDBUserRepository(db: db,
                                                 usernameToUser: usernameToUser,
                                                 usernameToWebsites: tokenToWebsites,
                                                 tokenToUsername: tokenToUsername)
    }

    def "tokenToUsername() should return username of user with given token"() {
        when:
            def actualUsername = userRepository.tokenToUsername(token)
        then:
            1 * tokenToUsername.getIfPresent(token) >> user.username
            actualUsername.equals(user.username)
    }

    def "createTokenForUser() should generate new token for user"() {
        when:
            def actualToken = userRepository.createTokenForUser(user.username)
        then:
            1 * tokenToUsername.put(_, user.username)
    }

    def "createUser() should create new user"() {
        when:
            def createdUser = userRepository.createUser(user)
        then:
            1 * usernameToUser.get(user.username) >> null
            1 * usernameToUser.put(user.username, user)
            1 * tokenToWebsites.put(_, [])
            1 * db.commit()
            createdUser.equals(user)
    }

    def "createUser() should return null if user already exists"() {
        when:
            def createdUser = userRepository.createUser(user)
        then:
            1 * usernameToUser.get(user.username) >> user
            createdUser.equals(null)
    }

    def "fetchUser() should fetch user object with given username"() {
        when:
            def actualUser = userRepository.fetchUser(user.username)
        then:
            1 * usernameToUser.get(user.username) >> user
            actualUser.equals(user)
    }

    def "validateLogin() should check whether user has introduced correct username & password"() {
        when:
            def password = user.password
            userRepository.createUser(user)
            def validLogin = userRepository.validateLogin(user.username, password)
        then:
            1 * usernameToUser.get(user.username) >> null
            1 * usernameToUser.put(user.username, user)
            1 * tokenToWebsites.put(_, [])
            1 * db.commit()
            1 * usernameToUser.get(user.username) >> user
            validLogin.equals(true)
    }
}

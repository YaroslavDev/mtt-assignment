package task.service

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import spock.lang.Specification
import task.repository.WebsiteRepository

import static task.Fixture.token
import static task.Fixture.website

class DefaultWebsiteServiceSpec extends Specification {

    private WebsiteRepository websiteRepository
    private RestTemplate restTemplate
    private WebsiteService websiteService

    def setup() {
        restTemplate = Mock()
        websiteRepository = Mock()
        websiteService = new DefaultWebsiteService(websiteRepository: websiteRepository, restTemplate: restTemplate)
    }

    def "fetchUserWebsites() should return list of users websites"() {
        when:
            def actualWebsites = websiteService.fetchUserWebsites(token)
        then:
            1 * websiteRepository.listWebsites(token) >> [website]
            1 * restTemplate.getForEntity(website.url, String.class) >> new ResponseEntity<String>(HttpStatus.OK)
            actualWebsites.equals([website])
            actualWebsites[0].status.equals(200)
    }

    def "fetchUserWebsites() should return list of users websites and 400 status if call failed"() {
        when:
            def actualWebsites = websiteService.fetchUserWebsites(token)
        then:
            1 * websiteRepository.listWebsites(token) >> [website]
            1 * restTemplate.getForEntity(website.url, String.class) >> new ResponseEntity<String>(HttpStatus.BAD_REQUEST)
            actualWebsites.equals([website])
            actualWebsites[0].status.equals(400)
    }
}

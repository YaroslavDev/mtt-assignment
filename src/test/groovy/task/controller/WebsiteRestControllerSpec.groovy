package task.controller

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.mock.web.MockServletContext
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification
import task.domain.Website
import task.repository.UserRepository
import task.repository.WebsiteRepository
import task.service.WebsiteService

import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static task.Fixture.user
import static task.Fixture.website
import static task.Fixture.token

@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
class WebsiteRestControllerSpec extends Specification {
    private MockMvc mvc
    private WebsiteRepository websiteRepository
    private UserRepository userRepository
    private WebsiteService websiteService

    def setup() {
        websiteRepository = Mock()
        userRepository = Mock()
        websiteService = Mock()
        mvc = MockMvcBuilders
                .standaloneSetup(new WebsiteRestController(websiteRepository: websiteRepository,
                                                           userRepository: userRepository,
                                                           websiteService: websiteService))
                .build()
    }

    def "GET /api/v1/websites should return list of user's sites"() {
        given:
            def request = MockMvcRequestBuilders.get("/api/v1/websites")
                                                .accept(APPLICATION_JSON)
                                                .header("Authorization", "Token ${token}")
        when:
            def result = mvc.perform(request).andExpect(status().isOk()).andReturn()
            def actualJson = new JsonSlurper().parseText(result.getResponse().getContentAsString())
            def actualSite = new Website(actualJson[0])
        then:
            1 * userRepository.tokenToUsername(token) >> user.username
            1 * websiteService.fetchUserWebsites(user.username) >> [website]
            actualSite.equals(website)
    }

    def "GET /api/v1/websites should return Unauthorized for non-existing token"() {
        given:
            def nonExistingToken = "non_existing_token"
            def request = MockMvcRequestBuilders.get("/api/v1/websites")
                                                .accept(APPLICATION_JSON)
                                                .header("Authorization", "Token ${nonExistingToken}")
        when:
            mvc.perform(request).andExpect(status().isUnauthorized()).andExpect(content().string(""))
        then:
            1 * userRepository.tokenToUsername(nonExistingToken) >> null
    }

    def "GET /api/v1/websites should return BadRequest for request without token"() {
        given:
            def request = MockMvcRequestBuilders.get("/api/v1/websites").accept(APPLICATION_JSON)
            mvc.perform(request).andExpect(status().isBadRequest())
    }

    def "POST /api/v1/websites should create new user site"() {
        given:
            def request = MockMvcRequestBuilders.post("/api/v1/websites")
                                                .contentType(APPLICATION_JSON)
                                                .content(JsonOutput.toJson(website))
                                                .header("Authorization", "Token ${token}")
        when:
            mvc.perform(request).andExpect(status().isCreated())
        then:
            1 * userRepository.tokenToUsername(token) >> user.username
            1 * websiteRepository.addWebsite(user.username, website)
    }

    def "POST /api/v1/websites should return Unauthorized for non-existing token"() {
        given:
            def nonExistingToken = "non_existing_token"
            def request = MockMvcRequestBuilders.post("/api/v1/websites")
                                                .contentType(APPLICATION_JSON)
                                                .content(JsonOutput.toJson(website))
                                                .header("Authorization", "Token ${nonExistingToken}")
        when:
            mvc.perform(request).andExpect(status().isUnauthorized())
        then:
            1 * userRepository.tokenToUsername(nonExistingToken) >> null
    }

    def "DELETE /api/v1/websites/{websiteId} should delete user's site"() {
        given:
            def request = MockMvcRequestBuilders.delete("/api/v1/websites/${website.id}")
                                                .header("Authorization", "Token ${token}")
        when:
            mvc.perform(request).andExpect(status().isNoContent())
        then:
            1 * userRepository.tokenToUsername(token) >> user.username
            1 * websiteRepository.removeWebsite(user.username, website.id)
    }

    def "DELETE /api/v1/websites/{websiteId} should return Unauthorized for non-existing token"() {
        given:
            def nonExistingToken = "non_existing_token"
            def request = MockMvcRequestBuilders.delete("/api/v1/websites/${website.id}")
                                                .header("Authorization", "Token ${nonExistingToken}")
        when:
            mvc.perform(request).andExpect(status().isUnauthorized())
        then:
            1 * userRepository.tokenToUsername(nonExistingToken) >> null
    }
}

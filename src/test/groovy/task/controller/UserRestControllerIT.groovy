package task.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.boot.test.TestRestTemplate
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate
import spock.lang.Specification
import task.Application
import task.domain.User
import task.domain.UserTokenDTO

import static task.Fixture.generateRandomUser

@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = Application.class)
@WebAppConfiguration
@IntegrationTest
class UserRestControllerIT extends Specification {
    @Value('${server.port}')
    private int port
    private RestTemplate template

    void setup() {
        template = new TestRestTemplate()
    }

    def "POST /register should register new user"() {
        setup:
            def user = generateRandomUser()
            ResponseEntity<UserTokenDTO> response =
                    template.postForEntity("http://localhost:${port}/api/v1/register", user, UserTokenDTO.class)
        expect:
            response.getStatusCode().value().equals(201)
            response.getBody().username.equals(user.username)
    }

    def "POST /register with invalid user should return BadRequest"() {
        setup:
            def user = generateRandomUser()
            user.password = ""
            ResponseEntity<String> response =
                template.postForEntity("http://localhost:${port}/api/v1/register", user, UserTokenDTO.class)
        expect:
            response.getStatusCode().value().equals(400)
            response.getBody().equals(null)
    }

    def "POST /register for already existing user should return Conflict"() {
        setup:
            def user = generateRandomUser()
            template.postForEntity("http://localhost:${port}/api/v1/register", user, UserTokenDTO.class)
            ResponseEntity<String> response =
                    template.postForEntity("http://localhost:${port}/api/v1/register", user, UserTokenDTO.class)
        expect:
            response.getStatusCode().value().equals(409)
            response.getBody().equals(null)
    }

    def "POST /login should successfully login existing user"() {
        setup:
            def user = generateRandomUser()
            template.postForEntity("http://localhost:${port}/api/v1/register", user, UserTokenDTO.class)
            ResponseEntity<UserTokenDTO> response =
                    template.postForEntity("http://localhost:${port}/api/v1/login", user, UserTokenDTO.class)
        expect:
            response.getStatusCode().value().equals(200)
            response.getBody().username.equals(user.username)
    }

    def "POST /login should return Unauthorized for non-existing user"() {
        setup:
            def user = new User(username: "unknown", email: "wrong", password: "wrong")
            ResponseEntity<String> response =
                template.postForEntity("http://localhost:${port}/api/v1/login", user, UserTokenDTO.class)
        expect:
            response.getStatusCode().value().equals(401)
            response.getBody().equals(null)
    }
}

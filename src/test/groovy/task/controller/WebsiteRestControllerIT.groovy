package task.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.boot.test.TestRestTemplate
import org.springframework.http.*
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate
import spock.lang.Specification
import task.Application
import task.domain.User
import task.domain.UserTokenDTO
import task.domain.Website

import static task.Fixture.generateRandomUser
import static task.Fixture.website

@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = Application.class)
@WebAppConfiguration
@IntegrationTest
class WebsiteRestControllerIT extends Specification {
    @Value('${server.port}')
    private int port
    private RestTemplate template
    private User user
    private String token

    def setup() {
        givenUserExistsAndHasOneWebsite()
    }

    def "GET /websites should return list of users websites"() {
        setup:
            ResponseEntity<List<Website>> response = fetchUserWebsites(token)
            def actualWebsites = response.getBody()
        expect:
            actualWebsites.size().equals(1)
            actualWebsites[0].id.equals(website.id)
            actualWebsites[0].name.equals(website.name)
            actualWebsites[0].url.equals(website.url)
    }

    def "GET /websites should return Unauthorized for wrong token"() {
        setup:
            ResponseEntity<List<Website>> response = fetchUserWebsites("wrong_token")
        expect:
            response.getStatusCode().value().equals(401)
    }

    def "POST /websites should add new website to user's websites"() {
        setup:
            def secondWebsite = new Website(id: 42, name: "example", url: "http://www.example.com/", status: 200)
            def createResponse = createUserWebsite(token, secondWebsite)
            def getResponse = fetchUserWebsites(token)
            def actualWebsites = getResponse.getBody()
        expect:
            createResponse.getStatusCode().value().equals(201)
            actualWebsites.size().equals(2)
            actualWebsites[1].id.equals(secondWebsite.id)
            actualWebsites[1].name.equals(secondWebsite.name)
            actualWebsites[1].url.equals(secondWebsite.url)
    }

    def "POST /websites should return Unauthorized for wrong token"() {
        setup:
            ResponseEntity<List<Website>> response = createUserWebsite("wrong_token", website)
        expect:
            response.getStatusCode().value().equals(401)
    }

    def "DELETE /websites/{websiteId} should remove user's website"() {
        setup:
            def deleteResponse = deleteUserWebsite(token, website.id)
            def getResponse = fetchUserWebsites(token)
            def actualWebsites = getResponse.getBody()
        expect:
            deleteResponse.getStatusCode().value().equals(204)
            actualWebsites.size().equals(0)
    }

    def "DELETE /websites/{websiteId} should return Unauthorized for wrong token"() {
        setup:
            ResponseEntity<List<Website>> response = deleteUserWebsite("wrong_token", website.id)
        expect:
            response.getStatusCode().value().equals(401)
    }

    /**
     * Fetches websites of user in current context
     * @param token user's token
     * @return ResponseEntity with List<Website>
     */
    private def fetchUserWebsites(String token) {
        def headers = new HttpHeaders()
        headers.set("Authorization", "Token ${token}")
        headers.setAccept([MediaType.APPLICATION_JSON])
        def entity = new HttpEntity(headers)
        ResponseEntity<List<Website>> response =
                template.exchange("http://localhost:${port}/api/v1/websites", HttpMethod.GET, entity, List.class)
        return response
    }

    /**
     * Adds new website to user's websites
     * @param token user's token
     * @param website new website
     * @return ResponseEntity that stores status code
     */
    private def createUserWebsite(String token, Website website) {
        def headers = new HttpHeaders()
        headers.set("Authorization", "Token ${token}")
        headers.setContentType(MediaType.APPLICATION_JSON)
        def entity = new HttpEntity(website, headers)
        ResponseEntity<String> response =
                template.postForEntity("http://localhost:${port}/api/v1/websites", entity, String.class)
        return response
    }

    /**
     * Delete existing user's website
     * @param token user's token
     * @param websiteId website's id
     * @return ResponseEntity that stores status code
     */
    private def deleteUserWebsite(String token, String websiteId) {
        def headers = new HttpHeaders()
        headers.set("Authorization", "Token ${token}")
        def entity = new HttpEntity(headers)
        ResponseEntity<String> response =
                template.exchange("http://localhost:${port}/api/v1/websites/${websiteId}", HttpMethod.DELETE, entity, String.class)
        return response
    }

    /**
     * Initial pre-condition step that create new user with single website.
     */
    private def givenUserExistsAndHasOneWebsite() {
        template = new TestRestTemplate()
        user = generateRandomUser()
        ResponseEntity<UserTokenDTO> response = template.postForEntity("http://localhost:${port}/api/v1/register", user, UserTokenDTO.class)
        token = response.getBody().token
        createUserWebsite(token, website)
    }
}

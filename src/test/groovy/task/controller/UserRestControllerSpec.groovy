package task.controller

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification
import task.domain.User
import task.domain.UserTokenDTO
import task.repository.UserRepository

import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static task.Fixture.token
import static task.Fixture.user
import static task.Fixture.registeredUser

class UserRestControllerSpec extends Specification {
    private MockMvc mvc
    private UserRepository userRepository

    def setup() {
        userRepository = Mock()
        mvc = MockMvcBuilders.standaloneSetup(new UserRestController(userRepository: userRepository)).build()
    }

    def "POST /register should register new user"() {
        given:
            def request = MockMvcRequestBuilders.post("/api/v1/register")
                                                .contentType(APPLICATION_JSON)
                                                .content(JsonOutput.toJson(user))
        when:
            def result = mvc.perform(request).andExpect(status().isCreated()).andReturn()
            def actualJson = new JsonSlurper().parseText(result.getResponse().getContentAsString())
            def actualUser = new UserTokenDTO(actualJson)
        then:
            1 * userRepository.createUser(user) >> registeredUser
            1 * userRepository.createTokenForUser(user.username) >> token
            actualUser.username.equals(user.username)
            actualUser.token.equals(token)
    }

    def "POST /register should return Conflict for existing user"() {
        given:
            def request = MockMvcRequestBuilders.post("/api/v1/register")
                                                .contentType(APPLICATION_JSON)
                                                .content(JsonOutput.toJson(user))
        when:
            mvc.perform(request).andExpect(status().isConflict())
        then:
            1 * userRepository.createUser(user) >> null
    }

    def "POST /register should return BadRequest for empty username or password"() {
        given:
            def invalidUser = new User(username: user.username, password: "")
            def request = MockMvcRequestBuilders.post("/api/v1/register")
                                                .contentType(APPLICATION_JSON)
                                                .content(JsonOutput.toJson(invalidUser))
            mvc.perform(request).andExpect(status().isBadRequest())
    }

    def "POST /login should login and return existing user"() {
        given:
            def request = MockMvcRequestBuilders.post("/api/v1/login")
                                                .contentType(APPLICATION_JSON)
                                                .content(JsonOutput.toJson(user))
        when:
            def result = mvc.perform(request).andExpect(status().isOk()).andReturn()
            def actualJson = new JsonSlurper().parseText(result.getResponse().getContentAsString())
            def actualUser = new UserTokenDTO(actualJson)
        then:
            1 * userRepository.fetchUser(user.username) >> registeredUser
            1 * userRepository.validateLogin(user.username, user.password) >> true
            1 * userRepository.createTokenForUser(user.username) >> token
            actualUser.username.equals(user.username)
            actualUser.token.equals(token)
    }

    def "POST /login should return Unauthorized for non-existing user"() {
        given:
            def request = MockMvcRequestBuilders.post("/api/v1/login")
                                                .contentType(APPLICATION_JSON)
                                                .content(JsonOutput.toJson(user))
        when:
            mvc.perform(request).andExpect(status().isUnauthorized())
        then:
            1 * userRepository.fetchUser(user.username) >> null
    }

    def "POST /login should return BadRequest for empty username or password"() {
        given:
            def invalidUser = new User(username: user.username, password: "")
            def request = MockMvcRequestBuilders.post("/api/v1/login")
                                                .contentType(APPLICATION_JSON)
                                                .content(JsonOutput.toJson(invalidUser))
            mvc.perform(request).andExpect(status().isBadRequest())
    }
}

package task

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

/**
 * Application's entry point
 */
@SpringBootApplication
class Application {
    static void main(String... args) {
        SpringApplication.run(Application.class, args)
    }
}

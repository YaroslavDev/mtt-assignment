package task.service

import task.domain.Website

/**
 * Service that contains business logic for working with user's websites
 */
interface WebsiteService {

    /**
     * Fetches user's websites and checks their status
     * @param token user's username
     * @return list of websites
     */
    List<Website> fetchUserWebsites(String username)
}

package task.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import task.domain.Website
import task.repository.WebsiteRepository

@Service
class DefaultWebsiteService implements WebsiteService {

    @Autowired
    private WebsiteRepository websiteRepository

    private RestTemplate restTemplate = new RestTemplate()

    @Override
    List<Website> fetchUserWebsites(String username) {
        def websites = websiteRepository.listWebsites(username)
        websites.collect {
            def status
            try {
                status = restTemplate.getForEntity(it.url, String.class).getStatusCode().value()
            } catch (Throwable e) {
                status = 400
            } finally {
                it.status = status
            }
            return it
        }
        return websites
    }
}

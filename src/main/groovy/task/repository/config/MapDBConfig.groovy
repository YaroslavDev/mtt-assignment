package task.repository.config

import com.google.common.cache.Cache
import com.google.common.cache.CacheBuilder
import com.google.common.cache.LoadingCache
import org.mapdb.DB
import org.mapdb.DBMaker
import org.mapdb.HTreeMap
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import javax.validation.Valid
import java.util.concurrent.TimeUnit

/**
 * Defines beans for configuration of MapDB and in-memory Cache structure
 */
@Configuration
class MapDBConfig {

    @Value('${task.db.file}')
    private String dbFile
    @Value('${task.db.encryption_password}')
    private String encryptionPassword
    @Value('${task.db.usernameToWebsitesCollection}')
    private String usernameToWebsitesCollection
    @Value('${task.db.usernameToUserCollection}')
    private String usernameToUserCollection
    @Value('${task.cache.lifetime}')
    private Integer cacheLifetime

    @Bean
    DB db() {
        return DBMaker
                .newFileDB(new File(dbFile))
                .closeOnJvmShutdown()
                .encryptionEnable(encryptionPassword)
                .make()
    }

    @Bean(name = ["usernameToWebsites"])
    HTreeMap usernameToWebsites() {
        HTreeMap treeMap
        if (db().exists(usernameToWebsitesCollection)) {
            treeMap = db().getHashMap(usernameToWebsitesCollection)
        } else {
            treeMap = db().createHashMap(usernameToWebsitesCollection).make()
        }
        return treeMap
    }

    @Bean(name = ["usernameToUser"])
    HTreeMap usernameToUser() {
        HTreeMap treeMap
        if (db().exists(usernameToUserCollection)) {
            treeMap = db().getHashMap(usernameToUserCollection)
        } else {
            treeMap = db().createHashMap(usernameToUserCollection).make()
        }
        return treeMap
    }

    @Bean
    Cache<String, String> cache() {
        return CacheBuilder.newBuilder()
                            .expireAfterAccess(cacheLifetime, TimeUnit.MINUTES)
                            .build()
    }
}

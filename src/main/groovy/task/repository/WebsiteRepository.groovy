package task.repository

import task.domain.Website

/**
 * Defines interface for accessing website data
 */
interface WebsiteRepository {

    /**
     * Return all user's websites
     * @param username user's username
     * @return List of [Website]
     */
    List<Website> listWebsites(String username)

    /**
     * Add new user website
     * @param username user's username
     * @param website new website
     */
    void addWebsite(String username, Website website)

    /**
     * Remove existing user website with given id
     * @param username user's username
     * @param websiteId existing website identifier
     */
    void removeWebsite(String username, String websiteId)
}
package task.repository

import org.mapdb.DB
import org.mapdb.HTreeMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import task.domain.Website

import javax.annotation.Resource

@Repository
class MapDBWebsiteRepository implements WebsiteRepository {

    @Autowired
    private DB db
    @Resource(name = "usernameToWebsites")
    private HTreeMap usernameToWebsites

    @Override
    List<Website> listWebsites(String username) {
        return (List<Website>) usernameToWebsites.get(username)
    }

    @Override
    void addWebsite(String username, Website website) {
        def websites = listWebsites(username)
        websites.add(website)
        usernameToWebsites.put(username, websites)
        db.commit()
    }

    @Override
    void removeWebsite(String username, String websiteId) {
        def websites = listWebsites(username)
        websites.removeAll { it.id == websiteId }
        usernameToWebsites.put(username, websites)
        db.commit()
    }
}

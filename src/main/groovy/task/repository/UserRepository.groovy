package task.repository

import task.domain.User

/**
 * Defines interface for accessing user data
 */
interface UserRepository {

    /**
     * Creates token for user and stores it in cache
     * @param username user's username
     * @return token
     */
    String createTokenForUser(String username)

    /**
     * Retrieves username of user with given token
     * @param token user's token
     * @return user's username
     */
    String tokenToUsername(String token)

    /**
     * Creates new user
     * @param username new username
     * @return created user
     */
    User createUser(User user)

    /**
     * Fetches user information by username
     * @param username user's username
     * @return user object
     */
    User fetchUser(String username)

    /**
     * Validates whether user introduces correct username & password
     * @param username user's username
     * @param password user's password
     * @return true or false
     */
    Boolean validateLogin(String username, String password)
}
package task.repository

import com.google.common.cache.Cache
import org.mapdb.DB
import org.mapdb.HTreeMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import task.domain.User

import javax.annotation.Resource
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom

@Repository
class MapDBUserRepository implements UserRepository {
    @Autowired
    private DB db
    @Resource(name = "usernameToUser")
    private HTreeMap usernameToUser
    @Resource(name = "usernameToWebsites")
    private HTreeMap usernameToWebsites
    @Autowired
    private Cache<String, String> tokenToUsername

    private Random random = new SecureRandom();

    @Override
    String tokenToUsername(String token) {
        def username = tokenToUsername.getIfPresent(token)
        return username
    }

    @Override
    String createTokenForUser(String username) {
        def token = generateToken()
        tokenToUsername.put(token, username)
        return token
    }

    @Override
    User createUser(User user) {
        if (usernameToUser.get(user.username)) {
            return null
        }
        def salt = Integer.toString(random.nextInt())
        user.password = makePasswordHash(user.password, salt)
        usernameToUser.put(user.username, user)
        usernameToWebsites.put(user.username, [])
        db.commit()
        return user
    }

    @Override
    User fetchUser(String username) {
        return (User) usernameToUser.get(username)
    }

    @Override
    Boolean validateLogin(String username, String password) {
        def user = (User) usernameToUser.get(username)
        def hashedAndSalted = user.password
        def salt = hashedAndSalted.split(",")[1]

        return hashedAndSalted == makePasswordHash(password, salt)
    }

    /**
     * Generate token as random UUID
     * @return UUID string
     */
    private static String generateToken() {
        return UUID.randomUUID().toString()
    }

    /**
     * Compute password hash in order to not store clear password in DB
     * @param password user submitted password
     * @param salt random string
     * @return hash
     */
    private static String makePasswordHash(String password, String salt) {
        try {
            String saltedAndHashed = password + "," + salt;
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(saltedAndHashed.getBytes());
            def encoder = Base64.getEncoder();
            def hashedBytes = (new String(digest.digest(), "UTF-8")).getBytes();
            return new String(encoder.encode(hashedBytes)) + "," + salt;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("SHA-256 is not available", e);
        }
    }
}

package task.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import task.domain.User
import task.domain.UserTokenDTO
import task.repository.UserRepository

/**
 * Defines REST API for user operations:
 * - register
 * - login
 */
@RestController
@RequestMapping("/api/v1")
class UserRestController {

    @Autowired
    private UserRepository userRepository

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    ResponseEntity<UserTokenDTO> register(@RequestBody User user) {
        if (user && user.username && user.email && user.password) {
            def registeredUser = userRepository.createUser(user)
            if (registeredUser) {
                def token = userRepository.createTokenForUser(user.username)
                def userTokenDTO = new UserTokenDTO(username: registeredUser.username, token: token)
                new ResponseEntity<UserTokenDTO>(userTokenDTO, HttpStatus.CREATED)
            } else new ResponseEntity<UserTokenDTO>(HttpStatus.CONFLICT)
        } else new ResponseEntity<UserTokenDTO>(HttpStatus.BAD_REQUEST)
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    ResponseEntity<UserTokenDTO> login(@RequestBody User user) {
        if (user && user.username && user.password) {
            def registeredUser = userRepository.fetchUser(user.username)
            if (registeredUser && userRepository.validateLogin(user.username, user.password)) {
                def token = userRepository.createTokenForUser(user.username)
                def userTokenDTO = new UserTokenDTO(username: registeredUser.username, token: token)
                new ResponseEntity<UserTokenDTO>(userTokenDTO, HttpStatus.OK)
            } else new ResponseEntity<UserTokenDTO>(HttpStatus.UNAUTHORIZED)
        } else new ResponseEntity<UserTokenDTO>(HttpStatus.BAD_REQUEST)
    }
}

package task.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import task.domain.Website
import task.repository.UserRepository
import task.repository.WebsiteRepository
import task.service.WebsiteService

/**
 * Defines REST API for user's websites operations:
 * - list websites
 * - add new website
 * - remove existing website
 */
@RestController
@RequestMapping("/api/v1")
class WebsiteRestController {

    @Autowired
    private WebsiteRepository websiteRepository
    @Autowired
    private UserRepository userRepository
    @Autowired
    private WebsiteService websiteService

    @RequestMapping(value = "/websites", method = RequestMethod.GET)
    ResponseEntity<List<Website>> listWebsites(@RequestHeader("Authorization") String authorization) {
        def username = extractUsername(authorization)
        if (username != null) new ResponseEntity<List<Website>>(websiteService.fetchUserWebsites(username), HttpStatus.OK)
        else new ResponseEntity<List<Website>>(HttpStatus.UNAUTHORIZED)
    }

    @RequestMapping(value = "/websites", method = RequestMethod.POST)
    ResponseEntity<String> addWebsite(@RequestHeader("Authorization") String authorization,
                                      @RequestBody Website website) {
        def username = extractUsername(authorization)
        if (username != null) {
            websiteRepository.addWebsite(username, website)
            new ResponseEntity<String>(HttpStatus.CREATED)
        } else return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED)
    }

    @RequestMapping(value = "/websites/{websiteId}", method = RequestMethod.DELETE)
    ResponseEntity<String> removeWebsite(@RequestHeader("Authorization") String authorization,
                                         @PathVariable String websiteId) {
        def username = extractUsername(authorization)
        if (username != null) {
            websiteRepository.removeWebsite(username, websiteId)
            new ResponseEntity<String>(HttpStatus.NO_CONTENT)
        } else new ResponseEntity<String>(HttpStatus.UNAUTHORIZED)
    }

    /**
     * Extracts username from token in authorization header value:
     * Example: Token b08435d5-e56b-4c8d-be59-aed0c695aca4
     * @param authorization value of Authorization HTTP header
     * @return username
     */
    private String extractUsername(String authorization) {
        def token = authorization.split()[1]
        def username = userRepository.tokenToUsername(token)
        return username
    }
}
package task.domain

import groovy.transform.EqualsAndHashCode
import org.springframework.stereotype.Component

/**
 * Stores information about user's website
 */
@EqualsAndHashCode
@Component
class Website implements Serializable {
    String id
    String name
    String url
    Integer status
}

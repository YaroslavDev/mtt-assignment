package task.domain

import groovy.transform.EqualsAndHashCode
import org.springframework.stereotype.Component

/**
 * Stores information about user
 */
@EqualsAndHashCode
@Component
class User implements Serializable {
    String username
    String email
    String password
}

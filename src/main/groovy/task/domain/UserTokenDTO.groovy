package task.domain

import groovy.transform.EqualsAndHashCode

/**
 * Data transfer object used for sending user token in JSON format
 */
@EqualsAndHashCode
class UserTokenDTO {
    String username
    String token
}
